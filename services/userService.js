const { UserRepository } = require('../repositories/userRepository');

class UserService {

    index() {
        return UserRepository.getAll();
    }
    
    show(id) {
        return UserRepository.getOne({id: id});
    }

    store(data) {
        return UserRepository.create(data) || null;
    }

    update(id, data) {
        return UserRepository.update(id, data);
    }

    remove(id) {
        return UserRepository.delete(id);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();