const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    index() {
        return FighterRepository.getAll();
    }
    
    show(id) {
        return FighterRepository.getOne({id: id});
    }

    store(data) {
        return FighterRepository.create(data) || null;
    }

    update(id, data) {
        return FighterRepository.update(id, data);
    }

    remove(id) {
        return FighterRepository.delete(id);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();