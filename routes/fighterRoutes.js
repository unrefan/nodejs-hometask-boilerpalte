const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { fighterValidated, createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function(req, res, next) {
    req.data = FighterService.index();

    next()
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    const user = FighterService.show(req.params.id);

    if (user) {
        req.data = user;
    } else {
        req.data = {
            error: true,
            status: 404,
            message: `User not found`,
        }
    }

    next();
}, responseMiddleware);

router.post('/', createFighterValid, fighterValidated, function(req, res, next) {
    if (!req.data.error) {
        req.data = FighterService.store(req.data);

        if (!req.data) {
            req.data = {
                error: true,
                message: 'Error during creating fighter'
            }
        }
    }

    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, fighterValidated, function(req, res, next) {
    if (!req.data.error) {
        req.data = FighterService.update(req.params.id, req.body);

        if (!req.data) {
            req.data = {
                error: true,
                message: 'Error during updating fighter'
            }
        }
    }

    next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
    req.data = FighterService.remove(req.params.id);

    if (!req.data) {
        req.data = {
            error: true,
            message: 'Error during deleting fighter'
        }
    }

    req.data = {message: 'Deleted'};

    next();
}, responseMiddleware);

module.exports = router;