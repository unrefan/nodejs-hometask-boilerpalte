const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid, userValidated } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function(req, res, next) {
    req.data = UserService.index();

    next()
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    const user = UserService.show(req.params.id);

    if (user) {
        req.data = user;
    } else {
        req.data = {
            error: true,
            status: 404,
            message: `User not found`,
        }
    }

    next();
}, responseMiddleware);

router.post('/', createUserValid, userValidated, function(req, res, next) {
    if (!req.data.error) {
        req.data = UserService.store(req.data);

        if (!req.data) {
            req.data = {
                error: true,
                message: 'Error during creating user'
            }
        }
    }

    next();
}, responseMiddleware);

router.put('/:id', updateUserValid, userValidated, function(req, res, next) {
    if (!req.data.error) {
        req.data = UserService.update(req.params.id, req.body);

        if (!req.data) {
            req.data = {
                error: true,
                message: 'Error during updating user'
            }
        }
    }

    next();
}, responseMiddleware);

router.delete('/:id', function(req, res, next) {
    req.data = UserService.remove(req.params.id);

    if (!req.data) {
        req.data = {
            error: true,
            message: 'Error during deleting user'
        }
    }

    req.data = {message: 'Deleted'};

    next();
}, responseMiddleware);

module.exports = router;