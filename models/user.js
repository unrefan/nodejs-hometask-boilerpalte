exports.user = {
    id: ['string'],
    firstName: ['required', 'string', 'min:3', 'max:255'],
    lastName: ['required', 'string', 'min:3', 'max:255'],
    email: ['required', 'string', 'email', 'regex:/@gmail.com$/'],
    phoneNumber: ['required', 'string', 'size:13', 'regex:/^[\+]380[0-9]{9}/'],
    password: ['required', 'string', 'min:3', 'max:255']
}