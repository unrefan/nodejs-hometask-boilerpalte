exports.fighter = {
    id: ['string'],
    name: ['required', 'string', 'min:3', 'max:255'],
    health: ['required', 'integer', 'size:100'],
    power: ['required', 'integer', 'between:0, 100'],
    defense: ['required', 'integer', 'between:1, 10'], // 1 to 10
}