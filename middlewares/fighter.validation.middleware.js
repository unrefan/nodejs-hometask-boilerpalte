const { fighter } = require('../models/fighter');
const { validator, validated } = require('./helpers/validate');

const createFighterValid = (req, res, next) => validator(req.body, fighter, {}, (err, status) => {
    if (!status) {
        req.data = {
            error: true,
            message: "Fighter entity to create isn't valid"
        }
        console.error(err);
    } else {
        req.data = {};
    }

    next();
});

const updateFighterValid = (req, res, next) => validator(req.body, fighter, {}, (err, status) => {
    if (!status) {
        req.data = {
            error: true,
            message: "Fighter entity to update isn't valid"
        };
        console.error(err);
    } else {
        req.data = {};
    }

    next();
});

const fighterValidated = (req, res, next) => {
    if (!req.data.error) {
        req.data = validated(req.body, fighter, ['id']);
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.fighterValidated = fighterValidated;