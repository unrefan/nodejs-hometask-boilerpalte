const response = require('./helpers/response');

const responseMiddleware = (req, res, next) => {
    if (req.data.error) {
        response.error(req, res);
    } else {
        response.success(req, res);
    }
}

exports.responseMiddleware = responseMiddleware;