module.exports = {
    error: (req, res) => {
        res.status(req.data.status || 400).send(JSON.stringify({
            error: true,
            message: req.data.message,
        }));
    },
    success: (req, res) => {
        res.status(200).send(JSON.stringify(req.data));
    }
}