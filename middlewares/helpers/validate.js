const Validator = require('validatorjs');
const validator = (body, rules, customMessages, callback) => {
    const validation = new Validator(body, rules, customMessages);
    validation.passes(() => callback(null, true));
    validation.fails(() => callback(validation.errors, false));
};

const validated = (body, rules, exlude = []) => {
    const effectiveBody = {};

    Object.keys(rules).forEach(key => {
        if (!exlude.some(excluedKey => excluedKey === key)) {
            effectiveBody[key] = body[key];
        }
    });

    return effectiveBody;
}

module.exports.validated = validated;
module.exports.validator = validator;
