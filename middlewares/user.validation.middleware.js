const { user } = require('../models/user');
const { validator, validated } = require('./helpers/validate');

const createUserValid = (req, res, next) => validator(req.body, user, {}, (err, status) => {
    if (!status) {
        req.data = {
            error: true,
            message: "User entity to create isn't valid"
        }
        console.error(err);
    } else {
        req.data = {};
    }

    next();
});

const updateUserValid = (req, res, next) => validator(req.body, user, {}, (err, status) => {
    if (!status) {
        req.data = {
            error: true,
            message: "User entity to update isn't valid"
        };
        console.error(err);
    } else {
        req.data = {};
    }

    next();
});

const userValidated = (req, res, next) => {
    if (!req.data.error) {
        req.data = validated(req.body, user, ['id']);
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.userValidated = userValidated;
